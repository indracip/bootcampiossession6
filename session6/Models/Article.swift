//
//  Article.swift
//  session6
//
//  Created by Citra Indra Pradita on 23/12/22.
//

import Foundation

struct ArticleResponse: Decodable {
    let articles: [Article]
}

struct Article: Decodable {
    let title: String
    let description: String?
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case description = "description"
    }
}
