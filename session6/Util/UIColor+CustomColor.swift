//
//  UIColor+CustomColor.swift
//  session6
//
//  Created by Citra Indra Pradita on 23/12/22.
//

import UIKit

extension UIColor {
    
    static let AppNavBarColor = UIColor(red: 209/255, green: 216/255, blue: 224/255, alpha: 1.0)
    
    static let AppNavBarTitleTextColor = UIColor(red: 56/255, green: 103/255, blue: 214/255, alpha: 1.0)
    
    static let AppNavBarLargeTitleTextColor = UIColor(red: 56/255, green: 103/255, blue: 214/255, alpha: 1.0)
}
